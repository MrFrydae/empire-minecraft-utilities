# Empire Minecraft Utilities
![Java 11](https://img.shields.io/badge/language-Java%2011-9B599A.svg?style=flat-square)
![Modrinth Downloads](https://img.shields.io/badge/dynamic/json?color=blue&label=Mod&ensp;Downloads&query=downloads&url=https%3A%2F%2Fapi.modrinth.com%2Fapi%2Fv1%2Fmod%2Femcutils)
[![GitHub license](https://img.shields.io/github/license/MrFrydae/Empire-Minecraft-Utilities?style=flat-square)](https://raw.githubusercontent.com/MrFrydae/Empire-Minecraft-Utilities/master/LICENSE)
![Environment: Client](https://img.shields.io/badge/environment-client-1976d2?style=flat-square)
![Version](https://img.shields.io/github/v/tag/MrFrydae/Empire-Minecraft-Utilities?label=version&style=flat-square)

A collection of small utilities for players of [Empire Minecraft](https://ref.emc.gs/GreenMeanie).  
Feature idea credits go to [Giselbaer](https://u.emc.gs/Giselbaer) and [wafflecoffee](https://u.emc.gs/wafflecoffee).

Made by [MrFrydae](https://u.emc.gs/GreenMeanie), and available under the MIT License.

Modrinth page for downloads: https://modrinth.com/mod/emcutils

*Note: No need to download jars for separate features. The releases each contain the feature it has added as well as all features previously released.*

## Feature List
* Chat channel buttons above the hotbar
* Automatic teleportation to a Residence on another server
* Easier viewing of usable Custom Items such as [Pot of Gold](https://wiki.emc.gs/pot-of-gold)
* Customizable tab list sorting
* Command aliases
* Vault Buttons
* And more to come :)

---
This mod is not sponsored by nor affiliated with Empire Minecraft, Starlis LLC, or Mojang Studios. It has been approved for use on Empire Minecraft in accordance with its [approved mod](https://mods.emc.gs) policies.
