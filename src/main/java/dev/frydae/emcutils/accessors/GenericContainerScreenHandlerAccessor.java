package dev.frydae.emcutils.accessors;

public interface GenericContainerScreenHandlerAccessor {
    void setRows(int rows);
}
